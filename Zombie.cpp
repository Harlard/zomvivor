#include "Zombie.h"
#include "TextureUtils.h"
#include "Animation.h"
#include "Game.h"
#include <stdexcept>
#include <iostream>

#include "SDL2Common.h"
#include "Sprite.h"
#include <string>

Zombie::Zombie():Sprite(){
  state = IDLE;
  speed = 75.0f;
  health = 100;
  point = 10;

  temp = nullptr;

  vectorOrientation = nullptr;

  targetRectangle.w = SPRITE_WIDTH;
  targetRectangle.h =  SPRITE_HEIGHT;

  player = nullptr;

}
void Zombie::init(SDL_Renderer *renderer, Vector2f position)
{
  string path("Assets/Images/zombie/zombieSprite.png");

  Sprite::init(renderer, path, 10, &position);

    animations[IDLE]->init(32, SPRITE_WIDTH,  SPRITE_HEIGHT, 1, 0);
   animations[WALK]->init(32, SPRITE_WIDTH,  SPRITE_HEIGHT, 1, 1);
    animations[RUN]->init(32, SPRITE_WIDTH,  SPRITE_HEIGHT, 1, 2);
    animations[SAUNTER]->init(32, SPRITE_WIDTH,  SPRITE_HEIGHT, 1, 3);
   animations[EATING]->init(24, SPRITE_WIDTH,  SPRITE_HEIGHT,  1, 4);
   animations[ATTACK1]->init(20, SPRITE_WIDTH,  SPRITE_HEIGHT,  1, 5);
   animations[ATTACK2]->init(20, SPRITE_WIDTH,  SPRITE_HEIGHT,  1, 6);
   animations[ATTACK3]->init(20, SPRITE_WIDTH,  SPRITE_HEIGHT,  1, 7);
   animations[DEATH1]->init(17, SPRITE_WIDTH,  SPRITE_HEIGHT,  1, 8);
   animations[DEATH2]->init(17, SPRITE_WIDTH,  SPRITE_HEIGHT,  1, 9);

   animations[DEATH1]->setLoop(false);
   animations[DEATH2]->setLoop(false);

   speed = (float)(rand()%(int)speed);

   vectorOrientation = new Vector2f();
   Sprite::collider->initCircle(&position, SPRITE_WIDTH, SPRITE_HEIGHT, 40);


      for(int i = 0; i <maxAnimations; i++){
        animations[i]->setMaxFrameTime(0.02f);
            }

}

Zombie::~Zombie(){
  delete player;
player = nullptr;
delete vectorOrientation;
vectorOrientation = nullptr;
delete temp;
temp = nullptr;
}


int Zombie::getCurrentAnimationState(){
  return state;
}

void Zombie::moveTo(Vector2f* target){
  temp = nullptr;
  temp = new Vector2f();
  temp->setX(target->getX());
  temp->setY(target->getY());
  temp->sub(position);
  vectorOrientation = temp;
  vectorOrientation->normalise();
  velocity = vectorOrientation;
  velocity->scale(speed);
}




void Zombie::ai(){

}

void Zombie::update( float timeDeltaInSeconds, Vector2f* globalVelocity){
/*  if(state != DEATH1) //don't move DEATH1 sprite
          ai();
      else
      {
          velocity->setX(0.0f);
          velocity->setY(0.0f);
      }*/
      velocity->zero();
      if(state != DEATH1)
      moveTo(player->getPosition());
      orientation = calculateOrientation(vectorOrientation);
      setState();
      Sprite::update(timeDeltaInSeconds, globalVelocity);
      Animation* current = animations[state];
      current->update(timeDeltaInSeconds);

     }



/*void Zombie::setGame(Game* game){
this->game = game;
}*/

void Zombie::takeDamage(int damage){
  health -= damage;

  if(!(health>0)){
    state = DEATH1;
  }
}


bool Zombie::isDead(){
  if (health > 0)
    return false;
  else
    return true;
}


int Zombie::getPoint(){
  return point;
}


void Zombie::draw(SDL_Renderer *renderer, SDL_Rect camara){


  Animation *current = this->animations[getCurrentAnimationState()];
  if(SDL_HasIntersection(&camara, &targetRectangle))
  SDL_RenderCopyEx(renderer, texture, current->getCurrentFrame(), &targetRectangle, orientation, nullptr, SDL_FLIP_NONE);

  }

float Zombie::calculateOrientation(Vector2f* direction){
  float angle =  atan2f(direction->getY(), direction->getX());
  angle *= (180.0f/3.142f);

  return angle+angleOffset;

}


void Zombie::setPlayer(Survivor* survivor){
  player = survivor;
}

void Zombie::setState(){
  float velocityF = velocity->lenght();
  if(velocityF == 0.0f){
    if(state != IDLE)
    state = IDLE;}
  if((velocityF > 0.0f)&&(velocityF<20.0f))
    {if(state != SAUNTER)
    state = SAUNTER;}
  if(velocityF >= 20.0f)
    {if(state != WALK)
    state = WALK;}
  }
  int Zombie::getState(){return state;}
