#ifndef SDL2COMMON_H_
#define SDL2COMMON_H_

#if defined(_WIN32) || defined(_WIN64)
    #include "SDL.h"
    #include "SDL_image.h"
#else
    #include "SDL2/SDL.h"
    #include "SDL2/SDL_image.h"
    #include "SDL2/SDL_ttf.h"
#endif

#endif
