#include "PowerUp.h"
#include "Survivor.h"
#include "Bullet.h"
#include "TextureUtils.h"
#include "Animation.h"
#include <stdexcept>

#include "SDL2Common.h"
#include "Sprite.h"
#include <string>
#include <iostream>
#include <cmath>



class Vector2f;
class Animation;
class Sprite;
class PowerUp;
class Game;

PowerUp::PowerUp():Sprite(){
  state = MAXHP;
}

void PowerUp::init(SDL_Renderer *renderer, Vector2f* position)
{
  string path("Assets/Images/Utils/PowerUpSprite.png");

  Sprite::init(renderer, path, 5, position);
        animations[MAXBULLET]->init(1, SPRITE_WIDTH,  SPRITE_HEIGHT, 1, 0);
        animations[MAXDMG]->init(1, SPRITE_WIDTH,  SPRITE_HEIGHT, 1, 1);
        animations[MAXHP]->init(1, SPRITE_WIDTH,  SPRITE_HEIGHT,  1, 2);
        animations[MAXSPD]->init(1, SPRITE_WIDTH,  SPRITE_HEIGHT,  1, 3);
        animations[MAXSTM]->init(1, SPRITE_WIDTH,  SPRITE_HEIGHT,  1, 4);

      for(int i = 0; i <maxAnimations; i++){
        animations[i]->setMaxFrameTime(100.0f);

            }

 Sprite::collider->initSquare(position, SPRITE_WIDTH, SPRITE_HEIGHT, SPRITE_WIDTH, SPRITE_HEIGHT);
}

PowerUp::~PowerUp()
{
  delete player;
  player = nullptr;
  delete game;
  game =  nullptr;

}


void PowerUp::update(float timeDeltaInSeconds, Vector2f* globalVelocity){


  Sprite::velocity->zero();

   Sprite::update(timeDeltaInSeconds, globalVelocity);
}


 int PowerUp::getCurrentAnimationState(){
       return state;
     }

void PowerUp::draw(SDL_Renderer *renderer){
  Animation *current = this->animations[getCurrentAnimationState()];
  Sprite::draw(renderer);
  }

void PowerUp::setState(int state){this->state = state;}
void PowerUp::setGame(Level* game){this->game = game;}
void PowerUp::setPlayer(Survivor* player){this->player = player;}
void PowerUp::givePower(){
  switch (state) {
    case MAXBULLET:
      player->incresedBulletLeft();
      break;
    case MAXDMG:
      player->incresedDamage();
      break;
    case MAXHP:
      player->incresedHP();
      break;
    case MAXSPD:
      player->incresedSpeed();
      break;
    case MAXSTM:
      player->incresedStamina();
      break;
  }
}
