#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "SDL2Common.h"
#include <stdexcept>
#include "Game.h"

const int SDL_OK = 0;
int main( int argc, char* args[] )
{



    int sdl_status = SDL_Init(SDL_INIT_EVERYTHING);

    if(sdl_status != SDL_OK){
      throw std::runtime_error("Error - SDL initialisation Failed\n");
      exit(1);
    }

    Game *game = new Game();
    game->initSDL();
    game->gameLoop();

    delete game;

    SDL_Quit();

    exit(0);
}
