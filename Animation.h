#ifndef ANIMATION_H_
#define ANIMATION_H_

#include "SDL2Common.h"


class Animation{
private:
  int maxFrame;
  int currentFrame;

  float accumulator;

  bool loop;

  SDL_Rect *frame;
  float frameTimeMax;


public:
  Animation();
  ~Animation();

  void init(           int noOfFrame,
                    const int SPRITE_WIDTH, const int SPRITE_HEIGHT,
                    int row, int col);

  SDL_Rect* getCurrentFrame();
  void update(float timeDelta);
  void setMaxFrameTime(float frameTimeMax);
  void setLoop(bool loopS);

};

#endif
