#ifndef MENU_H_
#define MENU_H_

#include "SDL2Common.h"
#include "Vector.h"
#include "Button.h"

class Button;
class Vector2f;
class Menu {
private:
  SDL_Texture* tittleAndBackground;
  int nextState;
  const Uint8 *keyStates;
  bool quit;
  int mX, mY;
  Vector2f* mousePos;
  Button* newGame;
  Button* exit;

public:
  Menu();
  ~Menu();
  void init(SDL_Renderer* renderer);
  void processInput();
  void draw(SDL_Renderer* renderer);
  int newState();
  void update();
  void buttonChecker();
  void escapeToExit();


};
#endif
