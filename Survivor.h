#ifndef SURVIVOR_H_
#define SURVIVOR_H_

#include "SDL2Common.h"
#include "Animation.h"
#include "Vector.h"
#include "Sprite.h"
#include "Collider.h"
#include "Level.h"


class Vector2f;
class Animation;
class Sprite;
class Collider;
class Level;



class Survivor : public Sprite
{
private:
  float cooldownTimer;
  static const float COOLDOWN_MAX;
  static const int SPRITE_WIDTH = 75;
  static const int SPRITE_HEIGHT = 55;
  float orientation;
  float angleOffset;
  Vector2f* vectorOrientation;
  Level* game;
  int bulletLeft, pistolMag, currentbulet, state, wState, score, maxBullet, maxHP, maxSTM, currentHP, currentSTM, damage, fatige, recoverSpeed;
  bool running;
public:
  Survivor();
  ~Survivor();
  enum SurvivorState{HGIDLE, HGMOVE, HGRELOAD, HGSHOT, RFLIDLE, RFLMOVE, RFLRELOAD,
    RFLSHOT, SGIDLE, SGMOVE, SGRELOAD, SGSHOT,  FLMELEE, HGMELEE, KNFMELEE, RFLMELEE, SGMELEE};
  enum WeaponState{HG, RFL, SG};
  void init(SDL_Renderer *renderer);
  void process(const Uint8 *keyStates, Vector2f* mousePos);
  void update(float timeDeltaInSeconds, Vector2f* mousePos, const Uint8 *keyStates,
  Vector2f* globalVelocity, Vector2f* endMap);
  int getCurrentAnimationState();
  void addScore(int newScore);
  void setGame(Level* level);
  int getBulletLeft();
  int getCurrentBullet();
  int getPistolMag();
  void setCurrentBullet(int nB);
  void reload();
  float calculateOrientation(Vector2f* direction);
  void moveForward();
  void moveBackward();
  Vector2f* getVelocity();
  void draw(SDL_Renderer* renderer);
  void incresedBulletLeft();
  void incresedDamage();
  void incresedHP();
  void incresedSpeed();
  void incresedStamina();
  int getDamage();
  int getCurrentHP();
  int getCurrentSTM();
  int getMaxHP();
  int getMaxSTM();
  int getScore();
};

#endif
