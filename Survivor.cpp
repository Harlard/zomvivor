#include "Survivor.h"
#include "TextureUtils.h"
#include "Animation.h"
#include <stdexcept>

#include "SDL2Common.h"
#include "Sprite.h"
#include <string>
#include <iostream>
#include <cmath>



class Vector2f;
class Animation;
class Sprite;
class Survivor;
class Game;

const float Survivor::COOLDOWN_MAX = 0.2f;


Survivor::Survivor():Sprite(){

  orientation = 0.0f;

  cooldownTimer = 0;

  vectorOrientation = nullptr;

  state = HGIDLE;
  wState = HG;

  targetRectangle.w = SPRITE_WIDTH;
  targetRectangle.h = SPRITE_HEIGHT;
  angleOffset = 0.0f;

  game = nullptr;

  bulletLeft = 45;
  currentbulet = 15;
  pistolMag = 15;
  maxBullet = 45;
  maxHP = 100;
  maxSTM = 100;
  currentHP = 100;
  currentSTM = 100;
  damage = 10;
  score = 0;
  running = false;
  fatige = 10;
  recoverSpeed = 20;

}

void Survivor::init(SDL_Renderer *renderer)
{
  string path("Assets/Images/survivor/survivorSprite25.png");

  Vector2f position(602.5f, 372.5f);
  vectorOrientation = new Vector2f();
  speed = 100.0f;


  Sprite::init(renderer, path, 14, &position);
        animations[HGIDLE]->init(20, SPRITE_WIDTH,  SPRITE_HEIGHT, 1, 0);
    animations[HGMOVE]->init(20, SPRITE_WIDTH,  SPRITE_HEIGHT, 1, 1);
   animations[HGRELOAD]->init(15, SPRITE_WIDTH,  SPRITE_HEIGHT,  1, 2);
   animations[HGSHOT]->init(3, SPRITE_WIDTH,  SPRITE_HEIGHT,  1, 3);
     animations[RFLIDLE]->init(20, SPRITE_WIDTH,  SPRITE_HEIGHT,  1, 4);
   animations[RFLMOVE]->init(20, SPRITE_WIDTH,  SPRITE_HEIGHT,  1, 5);
   animations[RFLRELOAD]->init(15, SPRITE_WIDTH,  SPRITE_HEIGHT,  1, 6);
   animations[RFLSHOT]->init(3, SPRITE_WIDTH,  SPRITE_HEIGHT,  1, 7);
   animations[SGIDLE]->init(20, SPRITE_WIDTH,  SPRITE_HEIGHT,  1, 8);
   animations[SGMOVE]->init(20, SPRITE_WIDTH,  SPRITE_HEIGHT,  1, 9);
   animations[SGRELOAD]->init(15, SPRITE_WIDTH,  SPRITE_HEIGHT,  1, 10);
   animations[SGSHOT]->init(3, SPRITE_WIDTH,  SPRITE_HEIGHT,  1, 11);


      for(int i = 0; i <maxAnimations; i++){
        animations[i]->setMaxFrameTime(100.0f);

            }

 Sprite::collider->initCircle(&position, SPRITE_WIDTH, SPRITE_HEIGHT, 50);
}

Survivor::~Survivor()
{
  delete vectorOrientation;
  vectorOrientation = nullptr;
  delete game;
  game = nullptr;

}



void Survivor::process( const Uint8 *keyStates, Vector2f* mousePos){

/*
    if(keyStates[SDL_SCANCODE_1])
      wState = HG;
    if(keyStates[SDL_SCANCODE_2])
      wState = RFL;
    if(keyStates[SDL_SCANCODE_3])
      wState = SG;
*/


switch (wState) {
  case HG:
  state = HGIDLE;
  break;
  case RFL:
  state = RFLIDLE;
  break;
  case SG:
  state = SGIDLE;
  break;
}
  vectorOrientation = mousePos;


  velocity->zero();
  running = false;

if(keyStates[SDL_SCANCODE_W] && keyStates[SDL_SCANCODE_S]){
  }
  else if(keyStates[SDL_SCANCODE_W]){
    if(keyStates[SDL_SCANCODE_LSHIFT])
      running = true;
    moveForward();
      switch (wState) {
      case HG:
      state = HGMOVE;
      break;
      case RFL:
      state = RFLMOVE;
      break;
      case SG:
      state = SGMOVE;
      break;
    }

    state  = HGMOVE;
  }else if(keyStates[SDL_SCANCODE_S]){
    moveBackward();
    switch (wState) {
      case HG:
      state = HGMOVE;
      break;
      case RFL:
      state = RFLMOVE;
      break;
      case SG:
      state = SGMOVE;
      break;
    }}

  if(keyStates[SDL_SCANCODE_R])
    reload();

}



void Survivor::update(float timeDeltaInSeconds, Vector2f* mousePos, const Uint8 *keyStates, Vector2f* globalVelocity, Vector2f* push){


  Sprite::velocity->zero();

   Sprite::update(timeDeltaInSeconds, globalVelocity);

   cooldownTimer += timeDeltaInSeconds;
   orientation = calculateOrientation(mousePos);
   Animation* current = animations[state];
   current->update(timeDeltaInSeconds);






   if((push->getX() !=  0) || (push->getY() !=  0))
  {  velocity->add(push);
  }else{
    process(keyStates, mousePos);

  }
  if((!running)&&(currentSTM<maxSTM)){
    if(recoverSpeed = 2000000000){
    ++currentSTM;
    recoverSpeed = 0;
}else{ ++recoverSpeed;}}}


 int Survivor::getCurrentAnimationState(){
       return state;
     }





void Survivor::addScore(int newScore){
  score+=newScore;
}

void Survivor::draw(SDL_Renderer *renderer){
  Animation *current = this->animations[getCurrentAnimationState()];

  SDL_RenderCopyEx(renderer, texture, current->getCurrentFrame(), &targetRectangle, orientation, nullptr, SDL_FLIP_NONE);

  }


float Survivor::calculateOrientation(Vector2f* direction){
  float angle =  atan2f(direction->getY(), direction->getX());
  angle *= (180.0f/3.142f);

  return angle+angleOffset;

}


void Survivor::moveForward(){
  Sprite::velocity->setX(vectorOrientation->getX());
  Sprite::velocity->setY(vectorOrientation->getY());
  Sprite::velocity->normalise();
    if((running==true)&&(currentSTM > 0)){
  Sprite::velocity->scale(speed * 3.0f);
  if(fatige = 900000000){
    --currentSTM;
    fatige = 0;}
    else{++fatige;}
  }else{
    Sprite::velocity->scale(speed);
  }
}
void Survivor::moveBackward(){

  Sprite::velocity->setX(vectorOrientation->getX());
  Sprite::velocity->setY(vectorOrientation->getY());

  Sprite::velocity->normalise();
  Sprite::velocity->scale(-speed/1.5);

}

Vector2f* Survivor::getVelocity(){ return velocity;}
void Survivor::setGame(Level* level){ game = level;}
int Survivor::getBulletLeft(){ return bulletLeft;}
int Survivor::getCurrentBullet(){  return currentbulet;}
int Survivor::getPistolMag(){ return pistolMag;}
void Survivor::setCurrentBullet(int nB){  currentbulet = nB;}
void Survivor::reload(){
  int temp = pistolMag - currentbulet;
  if(bulletLeft != 0){
      if(bulletLeft > pistolMag){
        currentbulet = pistolMag;
        bulletLeft -= temp;
      }else{
        currentbulet = bulletLeft;
        bulletLeft -= temp;
}}}

void Survivor::incresedBulletLeft(){maxBullet += 5; pistolMag += 1; bulletLeft = maxBullet; }
void Survivor::incresedHP(){maxHP += 10; currentHP = maxHP;}
void Survivor::incresedSpeed(){speed += 5;}
void Survivor::incresedStamina(){maxSTM += 10; currentSTM = maxSTM;}
void Survivor::incresedDamage(){damage += 10;}
int Survivor::getDamage(){return damage;}
int Survivor::getCurrentHP(){ return currentHP;}
int Survivor::getCurrentSTM(){ return currentSTM;}
int Survivor::getMaxHP(){return maxHP;}
int Survivor::getMaxSTM(){return maxSTM;}
int Survivor::getScore(){return score;}
