#include "Bullet.h"
#include "Animation.h"
#include "Vector.h"
#include "TextureUtils.h"
#include "Level.h"
#include <stdexcept>


#include <stdexcept>
#include <string>
#include <cmath>
#include <iostream>




Bullet::Bullet() : Sprite(){

  state = TRAVEL;
  speed = 200.0f;

  orientation = 0.0f;
  damage = 10;
  lifetime = 20.0f;
  angleOffset = -90.0f;


  targetRectangle.w = SPRITE_WIDTH;
  targetRectangle.h = SPRITE_HEIGHT;

  direction = nullptr;

}

Bullet::~Bullet(){
  delete direction;
  direction = nullptr;
}

void Bullet::init(SDL_Renderer *renderer, Vector2f* positionO, Vector2f* directionO){



  this->orientation = calculateOrientation(directionO);

  string path("Assets/Images/bullet.png");
  Vector2f* temp = new Vector2f();
  temp->setX(positionO->getX() + 30.5f); // not right
  temp->setY(positionO->getY() + 27.5f); // not right
  direction = new Vector2f();
  direction->setX(directionO->getX());
  direction->setY(directionO->getY());
  directionO->scale(30);
  velocity = new Vector2f();
  temp->add(directionO);
  std::cout << "direction"<< directionO->getX() << " " << directionO->getY() << " " << '\n';
  std::cout << "bullet"<<  temp->getX() << "p" << temp->getY() << " " << '\n';

  Sprite::init(renderer, path, 1, temp);





  animations[TRAVEL]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, 1, 0);

  for (int i = 0; i < maxAnimations; i++){
    animations[i]->setMaxFrameTime(0.2f);
  }

  Sprite::collider->initCircle(temp, SPRITE_WIDTH, SPRITE_HEIGHT, 20);


}

float Bullet::calculateOrientation(Vector2f* direction){
  float angle =  atan2f(direction->getY(), direction->getX());
  angle *= (180.0f/3.142f);
  return angle+angleOffset;

}

void Bullet::update(float timeDeltaInSeconds, Vector2f* globalVelocity){
  velocity->zero();
  lifetime -= timeDeltaInSeconds;
  velocity->setX(direction->getX());
  velocity->setY(direction->getY());
  velocity->normalise();
  velocity->scale(speed);
  Sprite::update(timeDeltaInSeconds, globalVelocity);
}


void Bullet::draw(SDL_Renderer *renderer){

  Animation *current = this->animations[getCurrentAnimationState()];

  SDL_RenderCopyEx(renderer, texture, current->getCurrentFrame(), &targetRectangle, orientation, nullptr, SDL_FLIP_NONE);

  }

  int Bullet::getCurrentAnimationState(){  return state; }
  void Bullet::setDamage(int nDamage){ damage = nDamage;}
  int Bullet::getDamage(){ return damage; }

  bool Bullet::hasExpired(){
    if(lifetime < 0.0f){return true;}
    else{return false;}
      }
