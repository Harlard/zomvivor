#include "Bar.h"
#include "SDL2Common.h"
#include "TextureUtils.h"

#include <iostream>


Bar::Bar(){
  SPRITE_WIDTH = 10;
  SPRITE_HEIGHT = 100;
  texture = nullptr;

}
Bar::~Bar(){
  delete position;
  position = nullptr;

  SDL_DestroyTexture(texture);
  texture = nullptr;

}

void Bar::init(SDL_Renderer *renderer, const char* path, int spritew, int spriteh, Vector2f* pos, int minValue, int maxValue ){
  position = new Vector2f(pos->getX(), pos->getY());
  SPRITE_WIDTH = spritew;
  SPRITE_HEIGHT = spriteh;
  tgtRect.w = SPRITE_WIDTH;
  tgtRect.h = SPRITE_HEIGHT;
  tgtRect.x = position->getX();
  tgtRect.y = position->getY();
  tgtValue.w = SPRITE_WIDTH;
  tgtValue.h = SPRITE_HEIGHT;
  tgtValue.x = 0;
  this->minValue = minValue;
  this->maxValue = maxValue;
  setBarInValues();

  texture =  CreateTextureFromFile(path, renderer);
}

void Bar::draw(SDL_Renderer *renderer){SDL_RenderCopy(renderer, texture, &tgtValue, &tgtRect);}
void Bar::update(int minValue, int maxValue){
  this->minValue = minValue;
  this->maxValue = maxValue;
  setBarInValues();
}

void Bar::setBarInValues(){
  pixToMove = (100-(minValue*100)/maxValue);
  tgtValue.y = (SPRITE_HEIGHT - pixToMove);
}
