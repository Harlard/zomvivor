#ifndef FLOOR_H_
#define FLOOR_H_

#include "SDL2Common.h"

#include <string>
#include "Vector.h"
#include "Collider.h"

class Collider;
class Vector2f;
class Floor{
private:
  SDL_Rect tgtFloor;
  SDL_Rect tgtCamara;
  Vector2f* position;
  SDL_Texture* texture;
  Vector2f* velocity;
  Collider* areaLimit;
  Vector2f* mapSize;
  Vector2f* temp;




public:
  Floor();
  ~Floor();
  void init(SDL_Renderer *renderer, const char* path, Vector2f* initPos, int mapX, int mapY);
  void draw(SDL_Renderer *renderer);
  void update(float timeDeltaInSeconds, Vector2f* globalVelocity);
  void applyRelativePosition(Vector2f* globalVelocity);
  SDL_Rect getTGT();
  Vector2f* endMap();

};

#endif
