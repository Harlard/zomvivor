#ifndef SPRITE_H_
#define SPRITE_H_

#include "SDL2Common.h"

#include <string>
#include "Vector.h"
#include "Animation.h"
#include "AABB.h"
using std::string;

class Vector2f;
class Animation;
class AABB;
class Sprite{
protected:
  SDL_Texture* texture;

  SDL_Rect targetRectangle;
  float speed;
  Vector2f* position;
  Vector2f* velocity;
  AABB *aabb;

  int maxAnimations;
  Animation** animations;
public:
  Sprite();

  virtual ~Sprite();
  virtual void init(SDL_Renderer *renderer,
                      string texturePath,
                    int maxAnimations,
                    Vector2f* initPos);

virtual void draw(SDL_Renderer *renderer);
virtual void update(float timeDeltaInSeconds);
virtual int getCurrentAnimationState()= 0;
virtual Vector2f* getPosition();
virtual void setPosition(Vector2f* newPosition);
virtual Vector2f* getCentrePosition();


virtual AABB* getAABB();
};
#endif
