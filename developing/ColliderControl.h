#ifndef COLLIDERCONTROL_H_
#define COLLIDERCONTROL_H_

#include "Collider.h"
#include "Survivor.h"
#include "Zombie.h"
#include "Bullet.h"
#include <vector>
using namespace;
class Collider;
class Survivor;
class Bullet;
class Zombie;
class ColliderControl{
private:
  vector<Zombie*> zombies;
  vector<Bullets*> bullets;
  Survivor* player;

public:
  ColliderControl();
  ~ColliderControl();
  void addPlayer(Survivor* survivorToAdd);
  void addZombie(Zombie* zombieToAdd);
  void addBullet(Bullet* bulletToAdd);
  Vector2f* pushBack();
  bool zombiePlayer();
  bool bulletZombie();
  void checkID();


}
