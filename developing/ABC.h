#ifndef ABC_H_
#define ABC_H_

#include "SDL2Common.h"
#include "Vector.h"
#include <vector>
#include <string>
using  std::vector;


class ABC{
private:
  Vector2f* position;
  vector<SDL_Rect> target;
  char* word;
  string path;
  char* word;
  vector<SDL_Texture*> textures;
public:
  ABC();
  ~ABC();
  void init(char* word, Vector2f* position);
  void wordComposer(char* word, Vector2f* position);
  void draw();
  void update();
};
#endif
