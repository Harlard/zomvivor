#include "Physics.h"
#include "Vector.h"


#include <stdexcept>

Physics::Physics(){
  speed = 0.0f;

  position = nullptr;
  velocity = nullptr;

}




Physics::~Physics(){
  delete position;
  position = nullptr;

  delete velocity;
  velocity = nullptr;

  }




void Physics::init(Vector2f* initPos){

  position = new Vector2f(initPos);
  velocity = new Vector2f();
  velocity->zero();


    }


void  Physics::update(float timeDeltaInSeconds)
 {
   Vector2f movement(velocity);
   movement.scale(timeDeltaInSeconds);

   position->add(&movement);

   targetRectangle.x = round(position->getX());
   targetRectangle.y = round(position->getY());

   current->update(timeDeltaInSeconds);

}

Vector2f* Physics::getPosition(){
  return position;
}

void Physics::setPosition(Vector2f* newPosition){
position->setX(newPosition->getX());
position->setY(newPosition->getY());
}
