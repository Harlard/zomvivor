#include "Sprite.h"
#include "Animation.h"
#include "Vector.h"
#include "TextureUtils.h"


#include <stdexcept>

Sprite::Sprite(){
  texture = nullptr;
  animations = nullptr;
  maxAnimations = 0;
}




Sprite::~Sprite(){
    for (int i = 0; i < maxAnimations; i++){

        delete animations[i];
        animations[i] = nullptr;
    }
    delete[] animations;
    animations = nullptr;

    SDL_DestroyTexture(texture);
    texture = nullptr;

}




void Sprite::init(SDL_Renderer *renderer, string texturePath, int maxAnimations){

  this->maxAnimations = maxAnimations;

  texture = CreateTextureFromFile(texturePath.c_str(), renderer);
   if (texture == nullptr)
    throw std::runtime_error("File not found");

    animations = new Animation*[maxAnimations];

    for (int i = 0; i < maxAnimations; i++)
    {
      animations[i] = new Animation();

    }
    }

void Sprite::draw(SDL_Renderer *renderer, int currentState, SDL_Rect *targetRectangle)
{
  Animation *current =  this->animations[currentState];

  SDL_RenderCopy(renderer, texture, current->getCurrentFrame(), &targetRectangle);
}
