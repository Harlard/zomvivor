#ifndef PHYSICS_H_
#define PHYSICS_H_

#include "SDL2Common.h"

#include <string>
#include "Vector.h"


class Vector2f;
class Physics{
protected:
  float speed;
  Vector2f* position;
  Vector2f* velocity;
  SDL_Rect targetRectangle;

public:
  Physics();

  virtual ~Physics();
  virtual void init(Vector2f* initPos);
  virtual void update(float timeDeltaInSeconds);
  virtual Vector2f* getPosition();
  virtual void setPosition(Vector2f* newPosition);
};
#endif
