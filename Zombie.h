#ifndef ZOMBIE_H_
#define ZOMBIE_H_

#include "SDL2Common.h"
#include "Sprite.h"

class Vector2f;
class Animation;
class Sprite;
class Collider;
class Survivor;

class Zombie : public Sprite {
private:
  int state;
  int health;
  int point;
  float orientation;
  float angleOffset;
  Vector2f* vectorOrientation;
  Survivor* player;
  Vector2f* temp ;
  static const int SPRITE_WIDTH = 146;
  static const int SPRITE_HEIGHT = 126;
public:
  Zombie();
  ~Zombie();

  enum NPCState{IDLE, WALK, RUN, SAUNTER, EATING, ATTACK1, ATTACK2, ATTACK3, DEATH1, DEATH2};

  void init(SDL_Renderer *renderer, Vector2f position);
  void update(float timeDeltaInSeconds, Vector2f* globalVelocity);
  void ai();
  void moveTo(Vector2f* target);
  void followPlayer();
  void takeDamage(int damage);
  bool isDead();
  void respawn(const int MAX_HEIGHT, const int MAX_WIDTH);
  int getPoint();
  void draw(SDL_Renderer* renderer, SDL_Rect camara);
  float calculateOrientation(Vector2f* direction);
  void setState();
  void setPlayer(Survivor* survivor);
  int getCurrentAnimationState();
  int getState();

};
 #endif
