#ifndef HUD_H_
#define HUD_H_

#include "SDL2Common.h"
#include "Bar.h"
//#include "Word.h"
#include "Survivor.h"
#include "Vector.h"

class Bar;
//class Word;
class Survivor;
class Vector2f;
class Hud{
private:
    Survivor* player;
    Bar* health;
    Bar* stamina;
  //  Word* score;
  /*  Word* currentBullet;
    Word* bulletLeft;*/
    int sco, cb, bl, chp, mhp, cst, mst;

public:
  Hud();
  ~Hud();
  void init(SDL_Renderer* renderer);
  void update(SDL_Renderer* renderer);
  void draw(SDL_Renderer* renderer);
  void updateBars();
//  void updateWords(SDL_Renderer* renderer);
  void setPlayer(Survivor* player);
  void setHud(SDL_Renderer* renderer);
};
#endif
