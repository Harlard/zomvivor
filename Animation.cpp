#include "SDL2Common.h"
#include "Animation.h"
#include <stdio.h>
#include <stdexcept>


Animation::Animation()
{
  maxFrame = 0;
  frame = nullptr;

  frameTimeMax = 0.4f;

  loop = true;

  currentFrame = 0;
  accumulator = 0;
}

void Animation::init(
  int noOfFrame,
   const int SPRITE_WIDTH, const int SPRITE_HEIGHT,
    int row, int col) {

      maxFrame = noOfFrame;

      frame = new SDL_Rect[maxFrame];
      for(int i = 0; i < maxFrame; i++){
        if(row == 1){
          frame[i].x = (i * SPRITE_WIDTH);
          frame[i].y = (col * SPRITE_HEIGHT);
        }else{
          if(col == 1)
                    {
                        for(int i = 0; i < maxFrame; i++){
                          frame[i].x = (row * SPRITE_WIDTH);
                          frame[i].y = (i * SPRITE_WIDTH);
                    }
                  }
                    else {
                      throw std::runtime_error("Bad paramters to init");
                    }
            }
frame[i].w = SPRITE_WIDTH;
frame[i].h = SPRITE_HEIGHT;
}

currentFrame = 0;
accumulator = 0.0f;
}

Animation::~Animation()
  {
    delete [] frame;
    frame = nullptr;
  }

SDL_Rect* Animation::getCurrentFrame(){
    return &frame[currentFrame];
  }

void Animation::update(float timeDeltaInSeconds){
    accumulator += timeDeltaInSeconds;
    if(accumulator > frameTimeMax){
      accumulator = 0.0f;

      if(currentFrame < maxFrame-1){
        currentFrame++;
      }else{
        if(loop == true)
        currentFrame = 0;
      }
    }
}
void Animation::setMaxFrameTime(float TimeMaxFrame){
  frameTimeMax = TimeMaxFrame;
}

void Animation::setLoop(bool loopS){
  loop = loopS;
}
