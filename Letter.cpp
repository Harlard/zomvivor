#include "Letter.h"
#include "SDL2Common.h"
#include "TextureUtils.h"
#include <string>

#include <iostream>


Letter::Letter(){
  texture = nullptr;
  SPRITE_WIDTH = 7;
  SPRITE_HEIGHT = 8;
}
Letter::~Letter(){
  delete position;
  position = nullptr;

  SDL_DestroyTexture(texture);
  texture = nullptr;

}

void Letter::init(SDL_Renderer *renderer, Vector2f* pos, char letter){
  this->letter = letter;
    position = new Vector2f(pos->getX(), pos->getY());
  tgtRect.w = SPRITE_WIDTH;
  tgtRect.h = SPRITE_HEIGHT;
  tgtRect.x = position->getX();
  tgtRect.y = position->getY();
  tgtValue.w = SPRITE_WIDTH;
  tgtValue.h = SPRITE_HEIGHT;
  setLetterInValues();

  texture =  CreateTextureFromFile("Assets/Images/Utils/ABC.png", renderer);
}

void Letter::draw(SDL_Renderer *renderer){SDL_RenderCopy(renderer, texture, &tgtValue, &tgtRect);}
void Letter::update(char letter){
  setLetterInValues();
}

void Letter::setLetterInValues(){
  int col = 0;
  int row = 0;
  //first col
  if(letter == ' ' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == '!' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == '"' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == '#' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == '$' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == '%' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  ++col;
  ++col;
  ++col;
  ++col;
  ++col;
  if(letter == '*' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == ',' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == '-' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == '.' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == '/' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == '0' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == '1' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  ++row; col = 0;
  if(letter == '2' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == '3' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == '4' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == '5' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == '6' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == '7' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == '8' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == '9' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == ':' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == ';' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == '<' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == '=' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == '>' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == '?' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'q' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'a' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'b' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'c' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  ++row; col = 0;
  if(letter == 'd' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'e' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'f' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'g' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'h' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'i' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'j' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'k' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'l' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'm' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'n' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'o' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'p' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'q' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'r' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 's' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 't' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'u' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  ++row; col = 0;
  if(letter == 'v' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'w' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'x' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'y' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'z' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == '[' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  ++col;
  if(letter == ']' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == '^' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == '_' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  ++col;
  if(letter == 'A' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'B' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'C' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'D' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'E' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'F' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'G' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  ++row; col = 0;
  if(letter == 'H' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'I' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'J' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'K' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'L' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'M' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'N' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'O' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'P' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'Q' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'R' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'S' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'T' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'U' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'V' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'W' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'X' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  if(letter == 'Y' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  ++row; col = 0;
  if(letter == 'Z' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;} ++col;
  ++col;
  ++col;
  ++col;
  if(letter == '"' ){tgtValue.x = col * SPRITE_WIDTH; tgtValue.y = row * SPRITE_HEIGHT;}
  else{tgtValue.x = 0; tgtValue.y = 0;}
}
