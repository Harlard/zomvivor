#include "Button.h"
#include "SDL2Common.h"
#include "TextureUtils.h"

#include <iostream>


Button::Button(){
  mousePos = nullptr;
  state = UNSELECT;
  SPRITE_WIDTH = 0;
  SPRITE_HEIGHT = 0;
  texture = nullptr;
  position = nullptr;
  click = false;

}
Button::~Button(){
  delete mousePos;
  mousePos = nullptr;

  delete position;
  position = nullptr;

  SDL_DestroyTexture(texture);
  texture = nullptr;

}

void Button::init(SDL_Renderer *renderer, const char* path, int spritew, int spriteh, Vector2f* pos){
  position = new Vector2f(pos->getX(), pos->getY());
  SPRITE_WIDTH = spritew;
  SPRITE_HEIGHT = spriteh;
  tgtRect.w = SPRITE_WIDTH;
  tgtRect.h = SPRITE_HEIGHT;
  tgtRect.x = position->getX();
  tgtRect.y = position->getY();
  tgtSpriteU.x = 0;
  tgtSpriteU.y = 0;
  tgtSpriteU.w = SPRITE_WIDTH;
  tgtSpriteU.h = SPRITE_HEIGHT;
  tgtSpriteS.x = 0;
  tgtSpriteS.y = SPRITE_HEIGHT;
  tgtSpriteS.w = SPRITE_WIDTH;
  tgtSpriteS.h = SPRITE_HEIGHT;

  texture =  CreateTextureFromFile(path, renderer);



}

void Button::draw(SDL_Renderer *renderer){
  switch (state) {
        case UNSELECT:
          SDL_RenderCopy(renderer, texture, &tgtSpriteU, &tgtRect);
          break;
        case SELECT:
          SDL_RenderCopy(renderer, texture, &tgtSpriteS, &tgtRect);
          break;

  }

}

void Button::processInput(int X, int Y, bool mouseClick){
  mouseOver( X, Y);
  if(state == SELECT)
    if(mouseClick == true)
      click = true;

}

void Button::mouseOver(int X, int Y){
  if((X  > (int)position->getX()) && (Y > (int)position->getY()) && (X  < ((int)position->getX()+ SPRITE_WIDTH)) && (Y < ((int)position->getY() + SPRITE_HEIGHT))){
      state = SELECT;
  }else{
      state = UNSELECT;
}
}

bool Button::clicked(){
  if(click == true)
    return true;
  else
    return false;
}
