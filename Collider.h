#ifndef COLIDER_H_
#define COLIDER_H_

#include "SDL2Common.h"
#include "Vector.h"

class Collider{
private:
  int ID;
  Vector2f* size;
  Vector2f* spriteSize;
  float radio;
  Vector2f* currentPosition;
  Vector2f* temp;
  Vector2f* temp1;
  Vector2f* temp2;
public:
  Collider();
  ~Collider();
  void initCircle(Vector2f* position, int sw, int sh, float r);
  void initSquare(Vector2f* position, int sw, int sh, float csw, float csh);
  bool checkCollision(Collider* other);
  bool circleCircle(Collider* other);
  bool circleSquare(Collider* other);
  bool squareCircle(Collider* other);
  bool squareSquare(Collider* other);
  void setCurrentPosition(Vector2f* position);
  Vector2f* getCurrentPosition();
  int checkID();
  float getRadio();
  Vector2f* getSize();

  enum IDcolider{CIRCLE, SQUARE};
};
#endif
