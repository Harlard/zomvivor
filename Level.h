#ifndef LEVEL_H_
#define LEVEL_H_

#include "SDL2Common.h"
#include "Floor.h"
#include "Survivor.h"
#include "Vector.h"
#include "Zombie.h"
#include "Bullet.h"
#include "Game.h"
#include "PowerUp.h"
#include "Bar.h"
#include "PowerUp.h"
//#include "Hud.h"
#include <vector>
using std::vector;

class Survivor;
class Floor;
class Vector2f;
class Zombie;
class Bullet;
class Game;
class Bar;
class PowerUp;
//class Hud;
class Level {
private:
  Floor* floor;
  Survivor* player;
  int nextState;
  const Uint8 *keyStates;
  bool quit;
  int mX, mY, zombiesN, mZombieN, chp, mhp, cst, mst, sco, cb, bl, wn, powerUpsInGame;
  Vector2f* mousePos;
  Vector2f* globalVelocity;
  vector<Zombie*> zombies;
  SDL_Rect camara;
  vector<Bullet*> bullets;
  vector<PowerUp*> powerUps;

  Game* game;
  Bar* health;
  Bar* stamina;
  //Hud* hud;


public:
  Level();
  ~Level();
  void init();
  void draw();
  void init(SDL_Renderer* renderer);
  void processInput();
  void draw(SDL_Renderer* renderer);
  int newState();
  void update(float timeDeltaInSeconds, float timeDelta);
  void mouseChecker();
  void createBullet();
  Survivor* getPlayer();
  void setGame(Game* gameO);
  void spawnZombie();
  void setRenderer(SDL_Renderer* renderer);
  void drawBullets(SDL_Renderer* renderer);
  void drawZombies(SDL_Renderer* renderer);
  void updateBullets(float timeDelta, Vector2f* globalVelocity);
  void updateZombies(float timeDelta, Vector2f* globalVelocity);
  void bulletChecker();
  void zombieChecker();
  void giveStat();
  void checkForChange();
  void createPowerUp(Vector2f* position);
  void checkPowerUps();
  void updatePowerUp(float timeDelta, Vector2f* globalVelocity);
  void drawPowerUp(SDL_Renderer* renderer);
  void updateBars();

  };
#endif
