#ifndef GAME_H_
#define GAME_H_

#include "SDL2Common.h"
#include "Menu.h"
#include "Level.h"

class Menu;
class Level;
class Game {
private:
  SDL_Window*	     gameWindow;
  SDL_Renderer*    gameRenderer;
  Menu* menu;
  Level* level;
  bool quit;
  int stateN;
  bool FT;
  const Uint8 *keyStates;

public:
  Game();
~Game();
void processinput();
void initSDL();
void gameLoop();
void sceneSwapper();
void sceneChoser(float timeDeltaInSeconds, float timeDelta);
void processInput();
void draw();
void cleanScreen();
void menuLoad();
SDL_Renderer* getRenderer();
void levelLoad(float timeDeltaInSeconds, float timeDelta);
enum GameState{QUIT, MENU, LEVEL};
static const int WINDOW_WIDTH = 1280;
static const int WINDOW_HEIGHT = 800;
GameState gameState;
};
#endif
//    std::cout << "check" << '\n';
