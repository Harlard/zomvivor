#include "Survivor.h"
//#include "Word.h"
#include "Bar.h"
#include "Vector.h"
#include "SDL2Common.h"
#include <string>
#include <iostream>



Hud::Hud(){
  player = nullptr;
  health = nullptr;
  stamina = nullptr;
/*  score = nullptr;
  currentBullet = nullptr;
  bulletLeft = nullptr;*/
  }

Hud::~Hud(){
  delete player;
  player = nullptr;
  delete health;
  health = nullptr;
  delete stamina;
  stamina = nullptr;
/*  delete score;
  score = nullptr;
  delete currentBullet;
  currentBullet = nullptr;
  delete bulletLeft;
  bulletLeft = nullptr;*/
}

void Hud::init(SDL_Renderer* renderer){
  this->player = player;
  health = new Bar();
  stamina = new Bar();
  /*score = new Word();
  currentBullet = new Word();
  bulletLeft = new Word();*/
  }

void Hud::update(SDL_Renderer* renderer){
  updateBars();
  /*updateWords(renderer);*/
}

void Hud::updateBars(){
  if((chp != player->getCurrentHP())||(mhp != player->getMaxHP())){
    chp = player->getCurrentHP();
    mhp = player->getMaxHP();
    health->update(chp, mhp);}
  if((cst != player->getCurrentSTM())||(mst != player->getMaxSTM())){
    cst = player->getCurrentSTM();
    mst = player->getMaxSTM();
    stamina->update(cst, mst);}
}
//void Hud::updateWords(SDL_Renderer* renderer){
  /*if(sco != player->getScore()){
    sco = player->getScore();
    std::string temp = std::__cxx11::to_string(sco);
    score->updateWord(renderer, temp);
  }
  if(cb != player->getCurrentBullet()){
    cb = player->getCurrentBullet();
    std::string temp1 = std::__cxx11::to_string(cb);
    currentBullet->updateWord(renderer, temp1);
  }
  if(bl != player->getBulletLeft()){
    bl = player->getBulletLeft();
    std::string temp2 = std::__cxx11::to_string(bl);
    bulletLeft->updateWord(renderer, temp2);



  }

}*/

void Hud::draw(SDL_Renderer* renderer){
  health->draw(renderer);
  stamina->draw(renderer);
  if(sco != player->getScore()){
    sco = player->getScore();
    system("clear");
    std::cout << sco << std::endl;
  }
/*  score->draw(renderer);
  currentBullet->draw(renderer);
  bulletLeft->draw(renderer);*/
}

void Hud::setPlayer(Survivor* player){
  this->player = player;
  sco = player->getScore();
}

void Hud::setHud(SDL_Renderer* renderer){
  Vector2f* position = new Vector2f(10, 200);
  chp = player->getCurrentHP();
  mhp = player->getMaxHP();
  health->init(renderer, "Assets/Images/Utils/healthBar.png", 10, 100, position, chp, mhp);
  position = new Vector2f(20, 200);
  cst = player->getCurrentSTM();
  mst = player->getMaxSTM();
  stamina->init(renderer, "Assets/Images/Utils/staminaBar.png", 10, 100, position, cst, mst);
  /*position = new Vector2f(600, 10);
  sco = player->getScore();
  std::string temp = std::__cxx11::to_string(sco);
  score->init(renderer, temp, position);
  position = new Vector2f(1080, 580);
  cb = player->getCurrentBullet();
  std::string temp1 = std::__cxx11::to_string(cb);
  currentBullet->init(renderer, temp1, position);
  position = new Vector2f(1080, 590);
  bl = player->getCurrentBullet();
  std::string temp2 = std::__cxx11::to_string(bl);
  bulletLeft->init(renderer, temp2, position);*/
}

//void Hud::convert int
