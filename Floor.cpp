#include "Floor.h"
#include "SDL2Common.h"
#include "TextureUtils.h"
#include "Vector.h"
#include "Collider.h"

#include <iostream>


Floor::Floor(){
  texture = nullptr;
  position = nullptr;
  velocity = nullptr;
  areaLimit = nullptr;
  mapSize = nullptr;


}
Floor::~Floor(){
  SDL_DestroyTexture(texture);
  texture = nullptr;
  delete position;
  position = nullptr;
  delete velocity;
  velocity = nullptr;
  delete areaLimit;
  areaLimit = nullptr;
  delete mapSize;
  mapSize = nullptr;
  delete temp;
  temp = nullptr;
}

void Floor::init(SDL_Renderer *renderer, const char* path, Vector2f* initPos, int mapX, int mapY){
  tgtFloor.w = 1280;
  tgtFloor.h = 800;
  tgtFloor.x = 0;
  tgtFloor.y = 0;
  tgtCamara.w = 1280;
  tgtCamara.h = 800;
  tgtCamara.x = ((int)initPos->getX());
  tgtCamara.y = ((int)initPos->getY());
  velocity = new Vector2f(0.0f, 0.0f);
  position = initPos;
  Vector2f* temp = new Vector2f(0.0f, 0.0f);
  temp->sub(initPos);
  texture =  CreateTextureFromFile(path, renderer);
  mapSize = new Vector2f((float)mapX, (float)mapY);
  areaLimit = new Collider;
  areaLimit->initSquare(temp,  mapX, mapY, (float)mapX, (float)mapY);
  delete temp;
  temp = nullptr;


}
void Floor::update(float timeDeltaInSeconds, Vector2f* globalVelocity){

  applyRelativePosition(globalVelocity);
  Vector2f movement(velocity);
  movement.scale(timeDeltaInSeconds);

  position->add(&movement);


  tgtCamara.x = (int)position->getX();
  tgtCamara.y = (int)position->getY();
  areaLimit->setCurrentPosition(position);



}

void Floor::draw(SDL_Renderer *renderer){
      SDL_RenderCopy(renderer, texture,  &tgtCamara, &tgtFloor);
}

void Floor::applyRelativePosition(Vector2f* globalVelocity){
  velocity->zero();
  velocity->add(globalVelocity);

}

SDL_Rect Floor::getTGT(){
  return tgtFloor;
}

Vector2f* Floor::endMap(){
  temp = nullptr;
  temp = new Vector2f();
  if((position->getX())<(0.0f))
    temp->setX(1.0f);
  if(((position->getX())+(1280.0f))>(mapSize->getX()))
    temp->setX(-1.0f);
  if((position->getY())<(0.0f))
    temp->setY(1.0f);
  if(((position->getY())+(800.0f))>(mapSize->getY()))
    temp->setY(-1.0f);

  return temp;

}
