#include <stdexcept>
#include <vector>
#include <iostream>
#include "Game.h"
#include "Menu.h"

using std::vector;

Game::Game(){
  gameWindow = nullptr;
  gameRenderer = nullptr;
  menu = nullptr;
  level = nullptr;
  gameState = MENU;
  keyStates = nullptr;
  stateN = 0;
  quit = false;
  FT = true;
}


void Game::initSDL(){
  gameWindow = SDL_CreateWindow("Awesome game",          // Window title
                            SDL_WINDOWPOS_UNDEFINED,  // X position
                            SDL_WINDOWPOS_UNDEFINED,  // Y position
                            WINDOW_WIDTH,
                            WINDOW_HEIGHT,                 // width, height
                            SDL_WINDOW_SHOWN);        // Window flags

  if(gameWindow != nullptr){

    gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);



 if(gameRenderer == nullptr){

      throw std::runtime_error("SDL could not create renderer\n");
    }
  }
    else { throw std::runtime_error("SDL could not create Window\n");
  }



}

Game::~Game(){
  if(menu != nullptr){
      delete menu;
      menu = nullptr;}

  if(level != nullptr){
      delete level;
      level = nullptr;}

  SDL_DestroyRenderer(gameRenderer);
  gameRenderer = nullptr;

  SDL_DestroyWindow(gameWindow);
  gameWindow = nullptr;
}


void Game::gameLoop(){
  unsigned int currentTimeIndex;
unsigned int preTimeIndex;
unsigned int timeDelta;
float timeDeltaInSeconds;
  while(!quit){


    currentTimeIndex = SDL_GetTicks();
    timeDelta = currentTimeIndex - preTimeIndex;
    timeDeltaInSeconds = timeDelta * 0.001f;


    preTimeIndex = currentTimeIndex;

cleanScreen();
sceneChoser(timeDeltaInSeconds, timeDelta);
SDL_RenderPresent(gameRenderer);
  }
}

void Game::sceneSwapper(){
  switch(stateN){
    case 0:
      gameState = QUIT;
      break;

    case 1:
      gameState = MENU;
      break;

    case 2:
      gameState = LEVEL;
      break;
    }

}


void Game::cleanScreen(){
  SDL_SetRenderDrawColor(gameRenderer, 0 ,0 ,0, 255);
  SDL_RenderClear(gameRenderer);
}


void Game::menuLoad(){
  if(FT == true) {
    menu = new Menu;
    menu->init(gameRenderer);
    FT = false;
  }
  if(menu->newState() !=1){
    stateN = menu->newState();
    sceneSwapper();
    delete menu;
    menu = nullptr;
    FT = true;
    }
    if(menu != nullptr){
      menu->update();
      menu->draw(gameRenderer);
    }

}

void Game::levelLoad(float timeDeltaInSeconds, float timeDelta){
  if(FT == true) {
    level = new Level;
    level->init(gameRenderer);
    level->setGame(this);
    FT = false;
  }
  if(level->newState() !=2){
    stateN = level->newState();
    sceneSwapper();
    delete level;
    level = nullptr;
    FT = true;
    }
    if(level != nullptr){
      level->update(timeDeltaInSeconds, timeDelta);
      level->draw(gameRenderer);
    }

}


void Game::sceneChoser(float timeDeltaInSeconds, float timeDelta){
  switch(gameState){
    case QUIT:
      quit = true;
      break;
    case MENU:
      menuLoad();
      break;
    case LEVEL:
      levelLoad(timeDeltaInSeconds, timeDelta);
      break;

  defaut:
  quit = true;
  break;

  }
}
SDL_Renderer* Game::getRenderer(){
  return gameRenderer;
}
