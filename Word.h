#ifndef WORD_H_
#define WORD_H_

#include "SDL2Common.h"
#include "Vector.h"
#include "Letter.h"
#include <vector>
#include <string>

using std::vector;

class Letter;
class Vector2f;
class Word{
private:
  int SPRITE_WIDTH;
  int SPRITE_HEIGHT;
  Vector2f* position;
  vector<Letter*> letters;
  std::string word;

public:
  Word();
  ~Word();
  void init(SDL_Renderer *renderer, std::string word, Vector2f* pos);
  void createWord(SDL_Renderer *rendererstd::string word);
  void updateWord(SDL_Renderer *renderer, std::string word);
  void draw(SDL_Renderer *renderer);
  };

#endif
