#include "Vector.h"
#include <iostream>

Vector2f::Vector2f(){
zero();
}

Vector2f::Vector2f(float x, float y){
  setX(x);
  setY(y);
}

Vector2f::Vector2f(Vector2f* other){
  x = other->x;
  y = other->y;
}

Vector2f::~Vector2f(){

}

void Vector2f::setX(float x){
  this->x = x;
}

float Vector2f::getX(){
  return x;
}

void Vector2f::setY(float y){
  this->y = y;
}

float Vector2f::getY(){
  return y;
}

void Vector2f::zero() {
  x = y = 0.0f;
  }

void Vector2f::add(Vector2f* other){
  x += other->getX();
  y += other->getY();
}

void Vector2f::scale(float scalar){
  x *= scalar;
  y *= scalar;
}

float Vector2f::lenght(){
  return sqrt((x*x) + (y*y));
}

void Vector2f::normalise(){
  float L = lenght();
  if(L > 1.0f){
    x /= L;
    y /= L;
  }}
 void Vector2f::sub(Vector2f* other){
   x -=other->getX();
   y -=other->getY();
 }
