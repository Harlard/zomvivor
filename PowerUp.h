#ifndef POWERUP_H_
#define POWERUP_H_

#include "SDL2Common.h"
#include "Animation.h"
#include "Vector.h"
#include "Sprite.h"
#include "Collider.h"
#include "Level.h"
#include "Survivor.h"


class Vector2f;
class Level;
class Survivor;
class Vector2f;
class PowerUp : public Sprite
{
private:
  static const int SPRITE_WIDTH = 50;
  static const int SPRITE_HEIGHT = 50;
  Survivor* player;
  Level* game;
  int state;

public:
  PowerUp();
  ~PowerUp();
  enum PowerUpState{MAXBULLET, MAXDMG, MAXHP, MAXSPD, MAXSTM};
  void init(SDL_Renderer *renderer, Vector2f* position);
  void update(float timeDeltaInSeconds, Vector2f* globalVelocity);
  int getCurrentAnimationState();
  void draw(SDL_Renderer* renderer);
  void setGame(Level* game);
  void setPlayer(Survivor* player);
  void setState(int state);
  void givePower();
};

#endif
