#include "TextureUtils.h"
#include "Menu.h"
#include "Button.h"

#include <string>
#include <iostream>



Menu::Menu(){
  tittleAndBackground = nullptr;
  keyStates = nullptr;
  nextState = 1;
  mX = 0;
  mY = 0;
  mousePos = nullptr;
  newGame = nullptr;
  exit = nullptr;

}

Menu::~Menu(){
  SDL_DestroyTexture(tittleAndBackground);
  tittleAndBackground = nullptr;

  delete mousePos;
  mousePos = nullptr;

  delete newGame;
  newGame = nullptr;

  delete exit;
  exit = nullptr;
  }

void Menu::init(SDL_Renderer* renderer){
  tittleAndBackground = CreateTextureFromFile("/home/berryjam/Zomvivor/zomvivor/Assets/Images/TittleAndBackground.png", renderer);
  keyStates = SDL_GetKeyboardState(NULL);
  mousePos = new Vector2f();
  Vector2f position(200.0f, 300.0f);
  newGame = new Button;
  newGame->init(renderer,
                "/home/berryjam/Zomvivor/zomvivor/Assets/Images/NewGame.png",
                330,
                58,
                &position
                );
  position = new Vector2f(200.0f, 358.0f);
  exit = new Button;
  exit->init(renderer,
                "/home/berryjam/Zomvivor/zomvivor/Assets/Images/Exit.png",
                110,
                58,
                &position);

}


void Menu::draw(SDL_Renderer* renderer){
    SDL_RenderCopy(renderer,
                tittleAndBackground,
              NULL, NULL);
    newGame->draw(renderer);
    exit->draw(renderer);

}


int Menu::newState(){
  return nextState;
}

void Menu::update(){
processInput();
}


void Menu::buttonChecker(){
  bool mouseClick = false;
  SDL_Event event;
  if(SDL_PollEvent(&event)){
    switch (event.type) {
      case SDL_QUIT:
      nextState = 0;
      break;
      case SDL_MOUSEBUTTONDOWN:
        if(event.button.button == SDL_BUTTON_LEFT){
          mouseClick = true;
      }

    }
}

  SDL_GetMouseState( &mX, &mY );
  newGame->processInput(mX, mY, mouseClick);
  exit->processInput(mX, mY, mouseClick);


  if(mouseClick == true){
    if(exit->clicked() == true)
      nextState = 0;
    if(newGame->clicked() == true)
      nextState = 2;
}
}

void Menu::escapeToExit(){

if(keyStates[SDL_SCANCODE_ESCAPE])
            nextState = 0;

}

void Menu::processInput(){
  buttonChecker();
  escapeToExit();


}
