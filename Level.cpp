#include "TextureUtils.h"
#include "Level.h"
#include "Floor.h"
#include "Button.h"
#include "Bullet.h"
//#include "Hud.h"

#include <string>
#include <vector>

#include <iostream>
using std::vector;

Level::Level(){
  floor = nullptr;
  keyStates = nullptr;
  nextState = 2;
  mX = 0;
  mY = 0;
  powerUpsInGame = 0;
  mousePos = nullptr;
  player = nullptr;
  globalVelocity = nullptr;
  game = nullptr;
  wn=0;

}

Level::~Level(){

  delete globalVelocity;
  globalVelocity = nullptr;

  delete floor;
  floor = nullptr;

  delete mousePos;
  mousePos = nullptr;

  delete player;
    player = nullptr;

  delete stamina;
  stamina = nullptr;

  delete health;
  health = nullptr;

  delete game;
  game =nullptr;

/*  delete hud;
  hud = nullptr;*/

  for (vector<Zombie*>::iterator it = zombies.begin();it != zombies.end();){
  delete *it;
  *it = nullptr;
  it = zombies.erase(it);}

  for (vector<Bullet*>::iterator it = bullets.begin();it != bullets.end();){
  delete *it;
  *it = nullptr;
  it = bullets.erase(it);}

  for (vector<PowerUp*>::iterator it = powerUps.begin();it != powerUps.end();){
  delete *it;
  *it = nullptr;
  it = powerUps.erase(it);}
  }

void Level::init(SDL_Renderer* renderer){
  player = new Survivor;
  player->init(renderer);
  player->setGame(this);
  floor = new Floor;
  floor->init(renderer, "/home/berryjam/Zomvivor/zomvivor/Assets/Images/level/playground.png", player->getPosition(), 2600, 2600);
  keyStates = SDL_GetKeyboardState(NULL);
  camara = floor->getTGT();
  mousePos = new Vector2f();
  player = new Survivor;
  player->init(renderer);
  globalVelocity = new Vector2f();
  health = new Bar();
  stamina = new Bar();
  chp = player->getCurrentHP();
  mhp = player->getMaxHP();
  Vector2f position = new Vector2f(10, 200);
  health->init(renderer, "Assets/Images/Utils/healthBar.png", 10, 100, &position, chp, mhp);
  position = new Vector2f(20, 200);
  cst = player->getCurrentSTM();
  mst = player->getMaxSTM();
  sco = player->getScore();
  cb = player->getCurrentBullet();
  bl = player->getBulletLeft();
  zombiesN = 0;
  mZombieN = 2;
  stamina->init(renderer, "Assets/Images/Utils/staminaBar.png", 10, 100, &position, cst, mst);

  /*hud = new Hud();
  hud->init(renderer);
  hud->setPlayer(player);
  hud->setHud(renderer);*/
}


void Level::draw(SDL_Renderer* renderer){
      floor->draw(renderer);
    player->draw(renderer);
    drawBullets(renderer);
    drawZombies(renderer);
    health->draw(renderer);
    stamina->draw(renderer);
    if(powerUpsInGame > 0)
    drawPowerUp(renderer);
//    hud->draw(renderer);

}


int Level::newState(){
  return nextState;
}

void Level::update(float timeDeltaInSeconds, float timeDelta){
processInput();
player->update(timeDelta, mousePos, keyStates, globalVelocity, floor->endMap());
globalVelocity = player->getVelocity();
floor->update(timeDeltaInSeconds, globalVelocity);
updateZombies(timeDeltaInSeconds, globalVelocity);
updateBullets(timeDeltaInSeconds, globalVelocity);
if(powerUpsInGame > 0){
updatePowerUp(timeDeltaInSeconds, globalVelocity);}
bulletChecker();
zombieChecker();
updateBars();
spawnZombie();
updateBars();
checkForChange();
//hud->update(game->getRenderer());
}

void Level::mouseChecker(){
  bool mouseClick = false;
  SDL_Event event;
  if(SDL_PollEvent(&event)){
    switch (event.type) {
      case SDL_QUIT:
      nextState = 0;
      break;
      case SDL_MOUSEBUTTONDOWN:
        if(event.button.button == SDL_BUTTON_LEFT){
          createBullet();
      }
      case SDL_KEYDOWN:
      switch (event.key.keysym.sym) {
        case SDLK_ESCAPE:
          nextState = 1;
          break;
          }

      break;
      case SDL_KEYUP:
        switch (event.key.keysym.sym){
          case SDLK_ESCAPE:
            break;
          case SDLK_t:
            break;

        }


    }
}

  SDL_GetMouseState( &mX, &mY );
  mousePos->setX(mX - 640);
  mousePos->setY(mY - 400);
  mousePos->normalise();

}



void Level::processInput(){
  mouseChecker();
}

void Level::createBullet(){
  int bulletInMag = player->getCurrentBullet();

  if(bulletInMag > 0){
    Bullet* bullet = new Bullet();

  bullet->init(game->getRenderer(),  player->getPosition(), mousePos);
  bullet->setDamage(player->getDamage());
  --bulletInMag;
  player->setCurrentBullet(bulletInMag);
  bullets.push_back(bullet);

}


}

Survivor* Level::getPlayer(){
  return player;
}

void Level::setGame(Game* gameO){
  game = gameO;
}

void Level::spawnZombie(){
  if(zombiesN == 0){
  mZombieN *= 2;
  ++wn;
  while(zombiesN < mZombieN){
  Zombie* zombie = new Zombie;
  Vector2f randomPos;

  int posX = 5000;
  int posY = 3000;

  int xCoord = rand()%posX - 2500;
  int yCoord = rand()%posY - 1500;

  if((xCoord < 640)&&(xCoord > 640))
      xCoord += 640;
  if((xCoord > -0)&&(xCoord < 640))
      xCoord += -640;

  if((yCoord < 800)&&(yCoord > 400))
      yCoord += 400;
  if((xCoord > -800)&&(xCoord < 400))
      yCoord += -400;

  randomPos.setX(xCoord);
  randomPos.setY(yCoord);


  zombie->init(game->getRenderer(), randomPos);
  zombie->setPlayer(player);
  zombies.push_back(zombie);
  ++zombiesN;

}}
}



void Level::drawZombies(SDL_Renderer* renderer){
  for(vector<Zombie*>::iterator it = zombies.begin(); it != zombies.end(); ++it){
  (*it)->draw(renderer, camara);}

}

void Level::updateZombies(float timeDelta, Vector2f* globalVelocity){
  for(vector<Zombie*>::iterator it = zombies.begin(); it != zombies.end(); ++it){
    (*it)->update(timeDelta, globalVelocity);
    }
}

void Level::drawBullets(SDL_Renderer* renderer){
  for(vector<Bullet*>::iterator it = bullets.begin(); it != bullets.end(); ++it){
  (*it)->draw(renderer);}

}

void Level::updateBullets(float timeDelta, Vector2f* globalVelocity){

  for(vector<Bullet*>::iterator it = bullets.begin(); it != bullets.end(); ++it){
    (*it)->update(timeDelta, globalVelocity);
    }

  for(vector<Bullet*>::iterator it = bullets.begin(); it != bullets.end();){
      if((*it)->hasExpired()){
        delete *it;
        *it = nullptr;
        it = bullets.erase(it);
      }else{
        ++it;
      }
      }

}

void Level::bulletChecker(){
  bool hit;
  for(vector<Bullet*>::iterator it = bullets.begin(); it != bullets.end();){
      hit = false;
    if(zombiesN > 0){
        for(vector<Zombie*>::iterator zombieToCheck = zombies.begin(); zombieToCheck != zombies.end();){
            if(!hit){
                  if((*it)->getCollider()->checkCollision((*zombieToCheck)->getCollider())){
                        (*zombieToCheck)->takeDamage((*it)->getDamage());
                        delete *it;
                        *it = nullptr;
                        it = bullets.erase(it);
                        hit = true;}
                  else{
                    ++zombieToCheck;
                  }
                }else{
                  ++zombieToCheck;
                }}if(!hit){
                  ++it;}
      }else{
      ++it;
    }
    }

}

void Level::zombieChecker(){

for(vector<Zombie*>::iterator zombieToCheck = zombies.begin(); zombieToCheck != zombies.end();){
  if((*zombieToCheck)->getState() != 8){
    if((*zombieToCheck)->getCollider()->checkCollision(player->getCollider())){
      ++zombieToCheck;
    }else{
      ++zombieToCheck;
        }
      }else if((*zombieToCheck)->getState() == 8)
    {
    player->addScore((*zombieToCheck)->getPoint());
    createPowerUp((*zombieToCheck)->getPosition());
    delete *zombieToCheck;
    *zombieToCheck = nullptr;
    zombieToCheck = zombies.erase(zombieToCheck);
    --zombiesN;
  }
}
}

void Level::checkPowerUps(){
  for(vector<PowerUp*>::iterator powerUpToCheck = powerUps.begin(); powerUpToCheck != powerUps.end();){
    if((*powerUpToCheck)->getCollider()->checkCollision(player->getCollider())){
      (*powerUpToCheck)->givePower();
      delete *powerUpToCheck;
      *powerUpToCheck = nullptr;
      powerUpToCheck = powerUps.erase(powerUpToCheck);
    }else{
      ++powerUpToCheck;
    }
}}

void Level::giveStat(){
system("clear");
std::cout << "WAVE Number "<< wn << std::endl;
std::cout << "Score - " << sco << std::endl;
std::cout << chp << "/" << mhp << " HP" << std::endl;
std::cout << cst << "/" << mst << " Stamina" << std::endl;
std::cout << cb << "/" << bl << " bullet" << std::endl;
}

void Level::checkForChange(){
  if((chp != player->getCurrentHP())||(mhp != player->getMaxHP())){
    chp = player->getCurrentHP();
    mhp = player->getMaxHP();
    giveStat();}

  if((cst != player->getCurrentSTM())||(mst != player->getMaxSTM())){
    cst = player->getCurrentSTM();
    mst = player->getMaxSTM();
    giveStat();}

  if(sco != player->getScore()){
      sco = player->getScore();
      giveStat();
  }
  if((bl != player->getBulletLeft())||(cb != player->getCurrentBullet())){
      bl = player->getBulletLeft();
      cb = player->getCurrentBullet();
      giveStat();
}}

void Level::createPowerUp(Vector2f* position){
  PowerUp* powerUp = new PowerUp();
  Vector2f* temp = new Vector2f(position->getX(), position->getY());
  int rp = 6;
  rp = (rand()%rp)-1;
  powerUp->init(game->getRenderer(), temp);
  powerUp->setState(rp);
  powerUps.push_back(powerUp);
  ++powerUpsInGame;
  delete temp;
  temp = nullptr;

}
void Level::updatePowerUp(float timeDelta, Vector2f* globalVelocity){
  for(vector<PowerUp*>::iterator it = powerUps.begin(); it != powerUps.end(); ++it){
    (*it)->update(timeDelta, globalVelocity);
    }
}
void Level::drawPowerUp(SDL_Renderer* renderer){
  for(vector<PowerUp*>::iterator it = powerUps.begin(); it != powerUps.end(); ++it){
  (*it)->draw(renderer);}

}

void Level::updateBars(){
  health->update(chp, mhp);
  stamina->update(cst, mst);
}
