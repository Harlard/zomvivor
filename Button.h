#ifndef BUTTON_H_
#define BUTTON_H_

#include "SDL2Common.h"
#include "Vector.h"
#include <string>


class Vector2f;
class Button{
private:
  Vector2f* mousePos;
  SDL_Texture* texture;
  int SPRITE_WIDTH;
  int SPRITE_HEIGHT;
  SDL_Rect tgtRect;
  SDL_Rect tgtSpriteU;
  SDL_Rect tgtSpriteS;
  Vector2f*  position;
  bool click;



public:
  Button();
  ~Button();
  void init(SDL_Renderer *renderer, const char* path, int spritew, int spriteh,
     Vector2f* pos);
  void mouseOver(int X, int Y);
  void processInput(int X, int Y, bool mouseClick);
  enum ButtonState{UNSELECT, SELECT};
  void draw(SDL_Renderer *renderer);
  bool clicked();
  ButtonState state;
};

#endif
