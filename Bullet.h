#ifndef BULLET_H_
#define BULLET_H_

#include <stdexcept>

#include "SDL2Common.h"
#include "Sprite.h"


class Vector2f;
class Animation;
class Level;


class Bullet : public Sprite{
private:
  int keyStates;
  static const int SPRITE_WIDTH = 16;
  static const int SPRITE_HEIGHT = 19;

  Vector2f* direction;

  int state;
  int damage;
  float lifetime;


  float orientation;

  float angleOffset;

public:
  Bullet();
  ~Bullet();

  enum BulletState{TRAVEL};

  void init(SDL_Renderer *renderer, Vector2f* positionO, Vector2f* directionO) ;
  void update(float timeDeltaInSeconds, Vector2f* globalVelocity);
  void draw(SDL_Renderer *renderer);
  float calculateOrientation(Vector2f* direction);
  int getCurrentAnimationState();
  int getDamage();
  void setDamage(int nDamage);
  bool hasExpired();
};
#endif
