#include "Vector.h"
#include "SDL2Common.h"
#include "Collider.h"
#include <iostream>

Collider::Collider(){
  currentPosition = nullptr;
  size = nullptr;
  spriteSize = nullptr;
  temp = nullptr;
  temp1 = nullptr;
  temp2 = nullptr;
  }

Collider::~Collider(){
  delete currentPosition;
  currentPosition = nullptr;
  delete size;
  size = nullptr;
  delete spriteSize;
  spriteSize = nullptr;
  delete temp;
  temp = nullptr;
  delete temp1;
  temp1 = nullptr;
  delete temp2;
  temp2 = nullptr;
}


void Collider::initCircle(Vector2f* position, int sw, int sh, float r){
  ID = CIRCLE;
  radio = r;
  spriteSize = new Vector2f((float)sw, (float)sh);
  currentPosition = new Vector2f();
  currentPosition->setX(position->getX()/2);
  currentPosition->setY(position->getY()/2);
}

void Collider::initSquare(Vector2f* position, int sw, int sh, float csw, float csh){
  ID = SQUARE;
  spriteSize = new Vector2f((float)sw, (float)sh);
  size = new Vector2f(csw, csh);
  currentPosition = new Vector2f();
  currentPosition->setX(position->getX() + ((sw - csw)/2));
  currentPosition->setY(position->getY() + ((sw - csw)/2));

}

bool Collider::checkCollision(Collider* other){
  bool collision;
  if((ID==CIRCLE)&&((other->checkID())==CIRCLE)){
    collision = circleCircle(other);
  }else if((ID==CIRCLE)&&((other->checkID())==SQUARE)){
    collision = circleSquare(other);
  }else if((ID==SQUARE)&&((other->checkID())==CIRCLE)){
    collision = squareCircle(other);
  }else if((ID==SQUARE)&&((other->checkID())==SQUARE)){
    collision = squareSquare(other);
  }
  return collision;
}

bool Collider::circleCircle(Collider* other){
  bool collision;
  temp = new Vector2f(currentPosition);
 temp->sub(other->getCurrentPosition());
  if(other == nullptr){collision = false;}
  if(((temp->lenght()) - radio - (other->getRadio())) < 0.0f )
    collision = true;
  else
    collision = false;
  return collision;
  temp = nullptr;
}

bool Collider::circleSquare(Collider* other){
  bool collision;
  temp1 = new Vector2f(other->getSize());
  temp2 = new Vector2f(other->getCurrentPosition());
  bool inX;
  bool inY;

  if(((currentPosition->getX()+radio)>(temp2->getX()))&&((currentPosition->getX()-radio)>(temp2->getX() + temp1->getX())))
    inX = true;
  if(((currentPosition->getY()+radio)>(temp2->getY()))&&((currentPosition->getY()-radio)>(temp2->getY() + temp1->getY())))
    inY = true;
  if((inX == true)&&(inY == true))
    collision = true;
  else
    collision = false;
    temp1 =nullptr;
    temp2 =nullptr;
  return collision;
    }

bool Collider::squareCircle(Collider* other){
  bool collision;
  Vector2f* temp2 = new Vector2f(other->getCurrentPosition());
  bool inX;
  bool inY;
  float ro = other->getRadio();
  if(((temp2->getX()+ro)>(currentPosition->getX()))&&((temp2->getX()-ro)>(currentPosition->getX() + size->getX())))
    inX = true;
  if(((currentPosition->getY()+ro)>(temp2->getY()))&&((currentPosition->getY()-ro)>(temp2->getY() + temp2->getY())))
    inY = true;
  collision = (inX && inY);
  temp2 = nullptr;
  return collision;
    }


bool Collider::squareSquare(Collider* other){
  bool collision;

  Vector2f* temp1 = new Vector2f(other->getSize());
  Vector2f* temp2 = new Vector2f(other->getCurrentPosition());
  bool inX;
  bool inY;
  if((temp2->getX()<(currentPosition->getX()+(size->getX())))||(currentPosition->getX()<(temp2->getX()+temp1->getX())))
   inX = true;
  if((temp2->getY()<(currentPosition->getY()+(size->getY())))||(currentPosition->getY()<(temp2->getY()+temp1->getY())))
    inX = true;
  collision = (inX && inY);
  temp1 = nullptr;
  temp2 = nullptr;
  return collision;
      }



int Collider::checkID(){
  return ID;
}

void Collider::setCurrentPosition(Vector2f* position){
  float x = position->getX();
  float y = position->getY();
  x += (spriteSize->getX()/2);
  y += (spriteSize->getY()/2);
if(ID == SQUARE){
  x -= (size->getX()/2);
  y -= (size->getY()/2);
  currentPosition->setX(x);
  currentPosition->setY(y);}
if(ID == CIRCLE)
  currentPosition->setX(x);
  currentPosition->setY(y);
}

Vector2f* Collider::getCurrentPosition(){
  return currentPosition;
}

float Collider::getRadio(){
  return radio;
}

  Vector2f* Collider::getSize(){
    return size;
  }
