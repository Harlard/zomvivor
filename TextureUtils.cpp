#include "TextureUtils.h"

//For printf
#include <stdio.h>


SDL_Texture* CreateTextureFromFile(const char* filename, SDL_Renderer *renderer)
{
    SDL_Texture*     texture = nullptr;

    SDL_Surface*     temp = nullptr;

    temp = IMG_Load(filename);

    if(temp == nullptr)
    {
        printf("%s image not found!", filename);
    }
    else
    {
        texture = SDL_CreateTextureFromSurface(renderer, temp);

        SDL_FreeSurface(temp);
        temp = nullptr;
    }

    return texture;
}
