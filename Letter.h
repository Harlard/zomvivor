#ifndef LETTER_H_
#define LETTER_H_

#include "SDL2Common.h"
#include "Vector.h"
#include <string>


class Vector2f;
class Letter{
private:
  SDL_Texture* texture;
  int SPRITE_WIDTH;
  int SPRITE_HEIGHT;
  SDL_Rect tgtRect;
  SDL_Rect tgtValue;
  Vector2f*  position;
  char letter;



public:
  Letter();
  ~Letter();
  void init(SDL_Renderer *renderer,  Vector2f* pos, char letter);
  void setLetterInValues();
  void draw(SDL_Renderer *renderer);
  void update(char letter);
  };

#endif
