#include "Sprite.h"
#include "Animation.h"
#include "Vector.h"
#include "TextureUtils.h"
#include "Collider.h"

#include <stdexcept>
#include <iostream>

Sprite::Sprite(){
  speed = 0.0f;

  position = nullptr;
  velocity = nullptr;

  texture = nullptr;
  collider = nullptr;
  animations = nullptr;

  maxAnimations = 0;
}




Sprite::~Sprite(){
  delete position;
  position = nullptr;

  delete velocity;
  velocity = nullptr;

  for (int i = 0; i < maxAnimations; i++){

    delete animations[i];
    animations[i] = nullptr;
}
delete[] animations;
animations = nullptr;

delete collider;
collider = nullptr;

    SDL_DestroyTexture(texture);
    texture = nullptr;

}




void Sprite::init(SDL_Renderer *renderer, string texturePath, int maxAnimations, Vector2f* initPos){

  this->maxAnimations = maxAnimations;

  position = new Vector2f(initPos);
  velocity = new Vector2f();
  velocity->zero();

  texture = CreateTextureFromFile(texturePath.c_str(), renderer);
   if (texture == nullptr)
    throw std::runtime_error("File not found");

    animations = new Animation*[maxAnimations];

    for (int i = 0; i < maxAnimations; i++)
    {
      animations[i] = new Animation();

    }
    collider = new Collider();

    }

void Sprite::draw(SDL_Renderer *renderer)
{
  Animation *current =  this->animations[getCurrentAnimationState()];

  SDL_RenderCopy(renderer, texture, current->getCurrentFrame(), &targetRectangle);
}

void  Sprite::update(float timeDeltaInSeconds, Vector2f* globalVelocity)
 {
   applyRelativePosition(globalVelocity);
   Vector2f movement(velocity);
   movement.scale(timeDeltaInSeconds);

   position->add(&movement);

   targetRectangle.x = round(position->getX());
   targetRectangle.y = round(position->getY());

   Animation *current = animations[getCurrentAnimationState()];

   current->update(timeDeltaInSeconds);

   if(collider != nullptr)
    collider->setCurrentPosition(position);
}

Vector2f* Sprite::getPosition(){
  return position;
}

Collider* Sprite::getCollider(){
  return collider;
}
void Sprite::setPosition(Vector2f* newPosition){
position->setX(newPosition->getX());
position->setY(newPosition->getY());
}

void Sprite::applyRelativePosition(Vector2f* globalVelocity){
  velocity->sub(globalVelocity);
}

SDL_Rect Sprite::getTGT(){
  return targetRectangle;
}
