#ifndef BAR_H_
#define BAR_H_

#include "SDL2Common.h"
#include "Vector.h"
#include <string>


class Vector2f;
class Bar{
private:
  SDL_Texture* texture;
  int SPRITE_WIDTH;
  int SPRITE_HEIGHT;
  SDL_Rect tgtRect;
  SDL_Rect tgtValue;
  Vector2f*  position;
  int minValue, maxValue;
  int pixToMove;



public:
  Bar();
  ~Bar();
  void init(SDL_Renderer *renderer, const char* path, int spritew, int spriteh,
     Vector2f* pos, int minValue, int maxValue);
  void setBarInValues();
  void draw(SDL_Renderer *renderer);
  void update(int minValue, int maxValue);
  };

#endif
