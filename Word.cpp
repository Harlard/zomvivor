#include "Word.h"
#include "Letter.h"
#include "SDL2Common.h"
#include <iostream>
#include <string>

Word::Word(){
  position = nullptr;
}
Word::~Word(){
  delete position;
  position = nullptr;

  for (vector<Letter*>::iterator it = letters.begin();it != letters.end();){
  delete *it;
  *it = nullptr;
  it = letters.erase(it);}
}
void Word::init(SDL_Renderer *renderer, std::string word, Vector2f* pos){
  position = new Vector2f(pos->getX(), pos->getY());
  strcpy(this->word, word);
  createWord(renderer);


}
void Word::createWord(SDL_Renderer* renderer){

  Vector2f* temp = new Vector2f(position->getX(), position->getY());
for(int i; word[i] == '\0'; i++){
  Letter* letter = new Letter();
  position->setX((temp->getX() + (i*7)));
  letter->init(renderer, temp, word[i]);

}
}
void Word::updateWord(SDL_Renderer* renderer, std::string word){
  this->word = word;
  for (vector<Letter*>::iterator it = letters.begin();it != letters.end();){
  delete *it;
  *it = nullptr;
  it = letters.erase(it);}
  createWord(renderer);

}
void Word::draw(SDL_Renderer *renderer){
  for(vector<Letter*>::iterator it = letters.begin(); it != letters.end(); ++it){
  (*it)->draw(renderer);}

}
