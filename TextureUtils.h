#ifndef TEXTURE_UTILS_H_
#define TEXTURE_UTILS_H_

#include "SDL2Common.h"

SDL_Texture* CreateTextureFromFile(const char* filename, SDL_Renderer *renderer);

#endif
